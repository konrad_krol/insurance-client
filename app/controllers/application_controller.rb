class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from Errno::ECONNREFUSED, with: :connection_refused
  
  def connection_refused
  	flash[:error] = 'Could not connect to external server!'
  	redirect_to :back
  end
end
