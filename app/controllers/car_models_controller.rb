class CarModelsController < ApplicationController
  before_filter :authenticate_user!
  # GET /car_models
  # GET /car_models.json
  def index
    @car_models = CarModel.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @car_models }
    end
  end

  # GET /car_models/1
  # GET /car_models/1.json
  def show
    @car_model = CarModel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @car_model }
    end
  end

  def synchronize
    CarModel.synchronize_all
    redirect_to car_models_path
  end
end
