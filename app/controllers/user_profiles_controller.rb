class UserProfilesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_user_profile!

  def edit
    user
  end

  def update
    user.update_attributes(params[:user])
    redirect_to root_path
  end

  private
  def user
    @user ||= User.find(params[:id])
  end

  def check_user_profile!
    redirect_to root_path if user != @user
  end
end