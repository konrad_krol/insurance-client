class Car < ActiveRecord::Base
  include Savon::Model
  document "http://localhost:8080/CarsWebService/CarsWebService?WSDL"

  belongs_to :car_model
  validates :car_model, :vin, :plates_number, :date_of_manufacture, presence: true
  validates :vin, uniqueness: true

  has_many :policies, dependent: :destroy

  state_machine :state, initial: :unsynchronized do
  	event :sync_completed do
      transition [:unsynchronized] => :synchronized
  	end

    event :local_modification do
      transition [:unsynchronized, :synchronized] => :unsynchronized
    end
  end

  def full_name
    "#{car_model_full_name} VIN: #{vin}"
  end

  def model_price
    car_model.price
  end

  def car_model_name
  	car_model.name
  end

  def car_model_manufacture_year
    car_model.manufacture_year
  end

  def car_model_full_name
    car_model.full_name
  end

  def self.synchronize_all
    Car.find_each { |car| car.synchronize }
  end

  def synchronize
    car_data = find_soap_request
    if car_data
      update_soap_request
    else
      create_soap_request
    end
    self.sync_completed
  end

  private
  def find_soap_request
    response = client.request(:ns2, :find_car_by_vin) do |soap|
      soap.body = { vin: vin }
    end
    response[:find_car_by_vin_response][:return]
  end
  def create_soap_request
    response = client.request(:ns2, :create_car) do |soap|
      soap.body = soap_request_parameters
    end
    response[:create_car_response][:return]
  end

  def update_soap_request
    response = client.request(:ns2, :update_car) do |soap|
      soap.body = soap_request_parameters
    end
    response[:update_car_response][:return]
  end

  def soap_request_parameters
    { car_model_name: car_model_name,
      car_model_manufacture_year: car_model_manufacture_year,
      vin: vin,
      plates_number: plates_number,
      date_of_manufacture: date_of_manufacture,
      updated_at: updated_at
    }
  end
end
