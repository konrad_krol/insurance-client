class CarModel < ActiveRecord::Base
  include Savon::Model
  document "http://localhost:8080/CarModelsWebService/CarModelsWebService?WSDL"

	def self.synchronize_all
    self.fetch_all.each do |car_model_data|
      car_model_data.delete(:id)
      if car_model = CarModel.find_by_name_and_manufacture_year(car_model_data[:name], car_model_data[:manufacture_year])
        car_model.update_attributes(car_model_data)
      else
        CarModel.create!(car_model_data)
      end
    end
	end

  def full_name
    "#{name} #{manufacture_year}"
  end

  private
  def self.fetch_all
    response = client.request(:ns2, :get_all_car_models)
    response_data = response[:get_all_car_models_response][:return]
    return [response_data] if response_data.is_a?(Hash)
    return [] if response_data.nil?
    return response_data
  end
end
