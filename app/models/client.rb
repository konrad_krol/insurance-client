class Client < ActiveRecord::Base
  include Savon::Model
  document "http://localhost:8080/ClientsWebservice/ClientsWebservice?WSDL"

  validates :first_name, :last_name, :address, :city, :postcode, :pesel, :gender, :birth_date, presence: true
  validates :pesel, uniqueness: true

  has_many :policies, dependent: :destroy

  state_machine :state, initial: :unsynchronized do
  	event :sync_completed do
      transition [:unsynchronized] => :synchronized
  	end

    event :local_modification do
      transition [:unsynchronized, :synchronized] => :unsynchronized
    end
  end

  def full_name
    "#{first_name} #{last_name} PESEL #{pesel}"
  end

  def self.synchronize_all
    Client.find_each { |client| client.synchronize }
  end

  def synchronize
    client_data = find_soap_request
    if client_data
      update_soap_request
    else
      create_soap_request
    end
    self.sync_completed
  end

  private
  def find_soap_request
    response = client.request(:ns2, :find_client_by_pesel) do |soap|
      soap.body = { pesel: pesel }
    end
    response[:find_client_by_pesel_response][:return]
  end
  def create_soap_request
    response = client.request(:ns2, :create_client) do |soap|
      soap.body = soap_request_parameters
    end
    response[:create_client_response][:return]
  end

  def update_soap_request
    response = client.request(:ns2, :update_client) do |soap|
      soap.body = soap_request_parameters
    end
    response[:update_client_response][:return]
  end

  def soap_request_parameters
    { first_name: first_name,
      last_name: last_name,
      address: address,
      city: city,
      postcode: postcode,
      pesel: pesel,
      gender: gender,
      birth_date: birth_date,
      license_number: license_number,
      updated_at: updated_at
    }
  end
end
