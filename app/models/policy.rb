class Policy < ActiveRecord::Base
  include Savon::Model
  document "http://localhost:8080/PolicyWebService/PolicyWebService?WSDL"

  NEW_CAR_RATE = 0.05
  UP_PER_YEAR_RATE = 0.003
  MAXIMAL_RATE = 0.15

  belongs_to :owner, class_name: 'Client', foreign_key: 'client_id'
  belongs_to :agent, class_name: 'User', foreign_key: 'agent_id'
  belongs_to :car

  validates :client_id, :car_id, presence: true
  validates :policy_number, uniqueness: true

  before_create :generate_policy_number
  before_save :calculate_total_price

  state_machine :state, initial: :unsynchronized do
    event :sync_completed do
      transition [:unsynchronized] => :synchronized
    end

    event :local_modification do
      transition [:unsynchronized, :synchronized] => :unsynchronized
    end
  end

  def full_agent_name
    agent.try(:full_name)
  end

  def car_full_name
    "#{car.full_name}"
  end

  def client_full_name
    "#{owner.first_name} #{owner.last_name}"
  end

  def client_pesel
    owner.pesel
  end

  def car_vin
    car.vin
  end

  def self.synchronize_all
    Policy.find_each { |policy| policy.synchronize }
  end

  def synchronize
    owner.synchronize
    car.synchronize
    policy_data = find_soap_request

    if policy_data
      update_soap_request
    else
      create_soap_request
    end
    self.sync_completed
  end

  private
  def find_soap_request
    response = client.request(:ns2, :find_policy_by_policy_number) do |soap|
      soap.body = { policy_number: policy_number }
    end
    response[:find_policy_by_policy_number_response][:return]
  end
  def create_soap_request
    response = client.request(:ns2, :create_policy) do |soap|
      soap.body = soap_request_parameters
    end
    response[:create_policy_response][:return]
  end

  def update_soap_request
    response = client.request(:ns2, :update_policy) do |soap|
      soap.body = soap_request_parameters
    end
    response[:update_policy_response][:return]
  end

  def soap_request_parameters
    { policy_number: policy_number,
      total_price: total_price,
      client_pesel: client_pesel,
      car_vin: car_vin,
      updated_at: updated_at
    }
  end

  def generate_policy_number
    self.policy_number = "AA#{Time.now.to_i}"
  end

  def calculate_total_price
    rate = (NEW_CAR_RATE + UP_PER_YEAR_RATE * car_age)
    rate = MAXIMAL_RATE if rate > MAXIMAL_RATE
    self.total_price = rate * car_model_price
  end

  def car_model_price
    car.model_price
  end

  def car_age
    Time.diff(Date.today, car.date_of_manufacture)[:year]
  end
end
