class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :postcode
      t.string :pesel
      t.string :gender
      t.date :birth_date
      t.string :license_number
      t.string :state

      t.timestamps
    end
  end
end
