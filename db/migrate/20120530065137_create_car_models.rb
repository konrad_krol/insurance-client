class CreateCarModels < ActiveRecord::Migration
  def change
    create_table :car_models do |t|
      t.string :name
      t.integer :manufacture_year
      t.float :price

      t.timestamps
    end
  end
end
