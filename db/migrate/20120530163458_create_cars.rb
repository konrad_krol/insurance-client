class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :car_model_id
      t.integer :vin
      t.string :plates_number
      t.date :date_of_manufacture

      t.timestamps
    end
  end
end
