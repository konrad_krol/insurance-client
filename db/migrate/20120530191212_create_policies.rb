class CreatePolicies < ActiveRecord::Migration
  def change
    create_table :policies do |t|
      t.integer :car_id
      t.integer :client_id
      t.string :policy_number
      t.float :total_price
      t.integer :agent_id

      t.timestamps
    end
  end
end
