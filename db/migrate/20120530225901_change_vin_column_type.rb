class ChangeVinColumnType < ActiveRecord::Migration
  def up
  	change_column :cars, :vin, :string
  end

  def down
  	change_column :cars, :vin, :integer
  end
end
